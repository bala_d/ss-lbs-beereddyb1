import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Wallet {
   /**
    * The RandomAccessFile of the wallet file
    */  
   private RandomAccessFile file;

   /**
    * Creates a Wallet object
    *
    * A Wallet object interfaces with the wallet RandomAccessFile
    */
    public Wallet () throws Exception {
	this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
    }

   /**
    * Gets the wallet balance. 
    *
    * @return                   The content of the wallet file as an integer
    */
    public int getBalance() throws IOException {
	this.file.seek(0);
	return Integer.parseInt(this.file.readLine());
    }

   /**
    * Sets a new balance in the wallet
    *
    * @param  newBalance          new balance to write in the wallet
    */
    public synchronized int safeWithdraw(int valueToWithdraw) throws Exception{
       int bal = getBalance();
        
          if(bal>=valueToWithdraw)
         {
          int newBalance = bal-valueToWithdraw;
          this.file.setLength(0);
           String str = new Integer(newBalance).toString()+'\n';
	   this.file.writeBytes(str);

	   return(valueToWithdraw);
	}
	else
        {

	this.file.setLength(0);
	String str = new Integer(0).toString()+'\n';
	this.file.writeBytes(str);

	return(bal);
	}
           
    }

	public synchronized void safeDeposit(int valueToDeposit) throws Exception
 	{
	if(valueToDeposit <=0)
	{
	throw new Exception("negative value, cannot deposit");
	}
	int bal = getBalance();
	int newBalance = bal + valueToDeposit;
	this.file.setLength(0);
	String str = new Integer(newBalance).toString()+'\n';
	this.file.writeBytes(str);
	}
	
   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
	this.file.close();
    }
}
