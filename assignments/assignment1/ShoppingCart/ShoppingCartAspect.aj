import java.util.Scanner;
import java.util.Date;
public aspect ShoppingCartAspect{
pointcut safeWithdraw(int price) :
call(* Wallet.safeWithdraw(int)) && args(price);
before(int price): safeWithdraw(price){
try{
Wallet wallet = new Wallet();
int balance = wallet.getBalance();
if (balance < price)
{
System.out.println(" You Don't have enough money");
}
}
catch(Exception e)
{
System.out.println(e);
}

}
after(int price) returning (int withdrawnAmount):
safeWithdraw( price) {
int balance;

try
{
Wallet wallet = new Wallet();
if(withdrawnAmount < price)
{
wallet.safeDeposit(withdrawnAmount);
System.out.println(" Amount is added back " +withdrawnAmount);
}
}
catch(Exception e)
{
System.out.println(e);
}
}
}
