import java.util.Date;
public aspect ShoppingCartAspect{
        after(int balance) : call(* Wallet.setBalance(int)) && args(balance){
        System.out.print("A Security Policy is enforced by BALA in AspectJ. The time now is " + (new Date()).toString());
        }
}
